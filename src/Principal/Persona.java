package Principal;


public class Persona {
    double estatura;
    double calzado;
    double peso;
    char sexo;
    double distancia;

    public Persona(double estatura, double calzado, double peso, char sexo,double distancia) {
        this.estatura = estatura;
        this.calzado = calzado;
        this.peso = peso;
        this.sexo = sexo;
        this.distancia = distancia;
    }
    
}
